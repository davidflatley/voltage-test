var gulp = require("gulp");
var sass = require("gulp-sass");
var order = require("gulp-order");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");

gulp.task("styles", function () {
	return gulp
		.src("src/sass/*.{css,scss}")
		.pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
		.pipe(concat("styles.css"))
		.pipe(gulp.dest("assets/css"));
});
gulp.task("scripts", function () {
	return gulp
		.src(["src/js/*.js", "node_modules/bootstrap/dist/js/bootstrap.js"])
		.pipe(order(["framework.js", "script1.js", "script2.js", "script3.js"]))
		.pipe(concat("scripts.js"))
		.pipe(uglify({ keep_fnames: true }))
		.pipe(gulp.dest("assets/js"));
});
gulp.task("watch", function () {
	gulp.watch("src/sass/*.scss", gulp.series("styles"));
	gulp.watch("src/js/*.js", gulp.series("scripts"));
});
